package com.example.restfulwebservices.module.user.service.filter;


public class UserFilter {
    private String name;

    public UserFilter() {

    }
    public UserFilter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
