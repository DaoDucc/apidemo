package com.example.restfulwebservices.module.user.api;


import com.example.restfulwebservices.common.ResponseDTO;
import com.example.restfulwebservices.common.header.header;
import com.example.restfulwebservices.common.header.headerList;
import com.example.restfulwebservices.common.header.headerObject;
import com.example.restfulwebservices.module.user.dto.model.User;
import com.example.restfulwebservices.module.user.service.UserService;
import com.example.restfulwebservices.module.user.service.filter.UserFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/users")
    ResponseEntity getAllUser(
            @RequestParam(required = false,defaultValue = "0") int offset,
            @RequestParam(required = false,defaultValue = "100") int limit,
            @RequestParam(required = false,defaultValue ="+id") String orderBy
    ) {
        int records=userService.getAllUser(orderBy).size();
        header header=new headerList(209,"Resource found",offset,limit,records);
        ResponseDTO userDTO=new ResponseDTO(header,userService.getAllUser(orderBy));
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping("/users/{id}")
    ResponseEntity getUserById(@PathVariable int id) {

            List<User> userList=new ArrayList<User>();
            userList.add((User) userService.getUserById(id));
        header header= new headerObject(209,"Resource found");
        ResponseDTO userDTO=new ResponseDTO(header, userList);
        return ResponseEntity.ok(userDTO);
    }
    @PostMapping("/users")
    public ResponseEntity createUser(@RequestBody User user) {
        List<User> userList=new ArrayList<>();
        userList.add((User) userService.createUser(user));
        header header=new headerObject(209,"Resource found");
        ResponseDTO userDTO=new ResponseDTO(header, userList);
        return ResponseEntity.ok(userDTO);
    }

    @PostMapping("/search/users")
    public ResponseEntity searchUser(
            @RequestParam(required = false,defaultValue = "0") int offset,
            @RequestParam(required = false,defaultValue = "100") int limit,
            @RequestParam(required = false,defaultValue ="+id") String orderBy,
            @RequestBody UserFilter userFilter) {
        List<User> userList= userService.getUserByName(orderBy,userFilter.getName());
        int records= userList.size();
        header header=new headerList(209,"Resource found",offset,limit,records);
        ResponseDTO userDTO=new ResponseDTO(header,userList);
        return ResponseEntity.ok(userDTO);
    }

    @PutMapping("/users")
    public ResponseEntity updateUser(@RequestBody User user) {
        List<User> userList=new ArrayList<User>();
        userList.add((User) userService.updateUser(user));
        header header=new headerObject(212,"resource edited");
        ResponseDTO userDTO=new ResponseDTO(header, userList);
        return ResponseEntity.ok(userDTO);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable int id) {
        this.userService.deleteUser(id);
        header header=new headerObject(211,"resource edited");
        List<User> userList=new ArrayList<User>();
        ResponseDTO userDTO=new ResponseDTO(header,userList);
        return ResponseEntity.ok(userDTO);
    }
}
