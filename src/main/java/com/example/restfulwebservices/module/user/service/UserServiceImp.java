package com.example.restfulwebservices.module.user.service;

import com.example.restfulwebservices.exception.InvalidParameterException;
import com.example.restfulwebservices.exception.ResourceNotFoundException;
import com.example.restfulwebservices.module.user.dto.model.User;
import com.example.restfulwebservices.module.user.service.db.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImp implements UserService<User> {

    @Autowired
    private UserRepository userRepository;

    // POST
    @Override
    public User createUser(User user) {
        return  userRepository.save(user);
    }
    // POST

    // PUT
    @Override
    public User updateUser(User user)  {
        Optional<User> userDb=this.userRepository.findById(user.getId());
        if (userDb.isPresent()) {
            User userUpdate = userDb.get();
            userUpdate.setName(user.getName());
            userRepository.save(userUpdate);
            return userUpdate;
        } else {
            throw new ResourceNotFoundException("recodsdsds"+ user.getId());
        }
    }

    // GET
    @Override
    public List<User> getAllUser(String orderBy) {
        if(!orderBy.substring(1).equals("name") &&
                !orderBy.substring(1).equals("id")){
            throw new InvalidParameterException("");
        }
        if(orderBy.substring(0,1).equalsIgnoreCase("-")) {
            return userRepository.findAll(Sort.by(Sort.Direction.DESC, orderBy.substring(1)));
        } else {
            return userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy.substring(1)));
        }
    }
    // GET{id}
    @Override
    public User getUserById(int userId)  {

        Optional<User> userDb = this.userRepository.findById(userId);

        if(userDb.isPresent()) {
            return userDb.get();
        } else {
            throw new ResourceNotFoundException( "not found");
        }
    }
    // POST / SEARCH by name
    @Override
    public List<User> getUserByName(String orderBy,String name) {
        if(!orderBy.substring(1).equals("name") &&
                !orderBy.substring(1).equals("id")){
            throw new InvalidParameterException("");
        }
        List<User> userList;
        if (orderBy.substring(0,1).equalsIgnoreCase("+")) {
            userList=this.userRepository.findUserByName(
                    Sort.by(Sort.Direction.ASC, orderBy.substring(1)), name);
        } else {
            userList=this.userRepository.findUserByName(
                    Sort.by(Sort.Direction.DESC, orderBy.substring(1)), name);
        }
        if(userList.size()>0) {
            return userList;
        } else {
            throw new ResourceNotFoundException("not found");
        }
    }
    // DELETE
    @Override
    public void deleteUser(int userId) {
        Optional<User> userDb =this.userRepository.findById(userId);
        if (userDb.isPresent()) {
            this.userRepository.delete(userDb.get());
        } else {
            throw new ResourceNotFoundException("Record not found delete");
        }
    }
}
