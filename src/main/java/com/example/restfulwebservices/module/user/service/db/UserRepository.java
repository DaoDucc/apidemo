package com.example.restfulwebservices.module.user.service.db;

import com.example.restfulwebservices.module.user.dto.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    // searching with name
    @Query("SELECT t FROM User t where t.name like %:name% ")
    List<User> findUserByName(Sort sort, @Param("name") String name);
}
