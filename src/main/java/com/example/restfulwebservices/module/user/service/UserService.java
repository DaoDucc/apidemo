package com.example.restfulwebservices.module.user.service;

import com.example.restfulwebservices.module.user.dto.model.User;

import java.util.List;

public interface UserService<T> {
    T createUser(User user);
    List<T> getAllUser(String orderBy);

    T getUserById(int userId);

    List<User> getUserByName(String orderBy,String name);
    void deleteUser(int userId);
    T  updateUser(User user);
}
