package com.example.restfulwebservices.exception;
import com.example.restfulwebservices.common.header.headerObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity handleAnyException(Exception e, WebRequest request) {
        headerObject header=new headerObject(500,"Unexpected errors in the back-end services");
        return new ResponseEntity(header, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity handleNotFoundException(Exception e, WebRequest request) {
        headerObject header=new headerObject(404,"resource not found");
        return new ResponseEntity(header, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(value = {InvalidParameterException.class})
    public ResponseEntity handleInvalidParameterException(Exception e,WebRequest request) {
        headerObject header=new headerObject(451,"Invalid parameters");
        return new ResponseEntity(header, HttpStatus.BAD_REQUEST);
    }
//    @ExceptionHandler(value = {HttpClientErrorException.class})
//    public ResponseEntity handleBadRequestException(Exception e,WebRequest request) {
//        headerObject header=new headerObject(452,"Invalid parameters");
//        return new ResponseEntity(header, HttpStatus.INVALID);
//

}
