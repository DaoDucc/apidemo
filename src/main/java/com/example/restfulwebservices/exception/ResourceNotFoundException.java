package com.example.restfulwebservices.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super();
    }

    public ResourceNotFoundException(String message,Throwable throwable) {
        super(message, throwable);
    }
}
