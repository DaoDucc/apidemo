package com.example.restfulwebservices.common;

import com.example.restfulwebservices.common.header.header;
import com.example.restfulwebservices.common.header.headerList;
import com.example.restfulwebservices.common.header.header;
import java.util.List;
public class ResponseDTO<T> {
    private header header;
    private List<T> body;

    public ResponseDTO() {

    }

    public ResponseDTO(header header, List<T> body) {
        this.header = header;
        this.body = body;
    }

    public header getHeader() {
        return header;
    }

    public void setHeader(header header) {
        this.header = header;
    }

    public List<T> getBody() {
        return body;
    }

    public void setBody(List<T> body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "header=" + header +
                ", body=" + body +
                '}';
    }
}
