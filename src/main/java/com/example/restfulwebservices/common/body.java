package com.example.restfulwebservices.common;
import java.util.List;

public class body<T> {

    private List<T> body;
    public body() {

    }

    public body(List<T> list) {
        this.body = list;
    }

    public List<T> getUserList() {
        return body;
    }

    public void setUserList(List<T> list) {
        this.body = list;
    }

    @Override
    public String toString() {
        return "body{" +
                "userList=" + body +
                '}';
    }
}
