package com.example.restfulwebservices.common.header;

public class headerList implements header {
    private int code;
    private String message;
    private int offset;
    private int limit;
    private int totalRecords;

    public headerList(int code, String message, int offset, int limit, int totalRecords) {
        this.code = code;
        this.message = message;
        this.offset = offset;
        this.limit = limit;
        this.totalRecords = totalRecords;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    @Override
    public String toString() {
        return "headerList{" +
                "offset=" + offset +
                ", limit=" + limit +
                ", totalRecords=" + totalRecords +
                '}';
    }
}
