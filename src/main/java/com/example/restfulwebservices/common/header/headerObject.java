package com.example.restfulwebservices.common.header;

public class headerObject implements header {
    private int code;
    private String message;

    public headerObject(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public headerObject() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "headerListDTO{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}