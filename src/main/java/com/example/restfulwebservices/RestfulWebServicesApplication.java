package com.example.restfulwebservices;


import com.example.restfulwebservices.module.user.service.db.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class RestfulWebServicesApplication {

    public static void main(String[] args) {

        SpringApplication.run(RestfulWebServicesApplication.class, args);

    }
}
